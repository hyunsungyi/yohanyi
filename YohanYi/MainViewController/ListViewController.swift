//
//  ListViewController.swift
//  YohanYi
//
//  Created by Yohan Yi on 2/12/23.
//

import UIKit

class ListViewController: UIViewController, UISearchBarDelegate {
    
    private var list = [School]()
    private var searchedList = [School]()
    private var tableView: UITableView!
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    private let searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = Constant.mainTitle

        setupTableView()
        setupSearchBar()
        setupActivityIndicator()
        fetchList()
    }
    
    private func setupActivityIndicator() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func setupSearchBar() {
        let searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: 0, width: 200, height: 70)
        searchBar.delegate = self
        searchBar.showsCancelButton = false
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = "try ESSEX STREET ACADEMY...."
        searchBar.sizeToFit()
        tableView.tableHeaderView = searchBar
    }
    
    private func setupTableView() {
        tableView = UITableView(frame: view.bounds)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(ListCell().classForCoder, forCellReuseIdentifier: ListCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        view.addSubview(tableView)
    }
    
    private func fetchList() {
        activityIndicator.startAnimating()
        WebServiceRequest.fetchingNYCSchoolsList() { success, errorMessage, data in
            if success, let data = data as? [School] {
                self.list = data
                self.searchedList = data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                print("Error :: \(String(describing: errorMessage))")
            }
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedList = searchText.isEmpty ? list : list.filter({(dataString: School) -> Bool in
            return dataString.schoolName?.range(of: searchText, options: .caseInsensitive) != nil
        })
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ListCell.identifier) as? ListCell else { return UITableViewCell() }
        cell.configurateTheCell(searchedList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolDbn = searchedList[indexPath.row].dbn
        let detailVC = DetailViewController()
        detailVC.schoolDbn = schoolDbn
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
