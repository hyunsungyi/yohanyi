//
//  Constants.swift
//  YohanYi
//
//  Created by Yohan Yi on 2/12/23.
//

import Foundation

struct Constant {
    static let mainTitle = "NYC Schools"
    static let detailVCTitle = "SAT Score Detail"

    static let shoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let satListUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    static let mainColor = "763626"
    static let subColor = "2A3132"
    static let subColor2 = "336B87"
    static let backgroundColor = "90AFC5"
}

enum WebserviceType {
    case NYCSchoolsList
    case SATScoreList
}

enum TestType {
    case math
    case writing
    case reading
}
