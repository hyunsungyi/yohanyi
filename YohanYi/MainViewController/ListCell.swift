//
//  ListCell.swift
//  YohanYi
//
//  Created by Yohan Yi on 2/12/23.
//

import UIKit
import MapKit

// ListCell UI Overview
// ┌─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
// │     contentView                                                                                                 │
// │┌───────────────────────────────────────────────────────────────────────────────────────────────────────────────┐│
// ││    boxView                                                                                                    ││
// ││┌─────────────────────────────────────────────────────────────────────────────────────────────────────────────┐││
// │││   verticalStackView                                                                                         │││
// │││ ┌─────────────────────────────────────────────────────────────────────────────────────────────────────────┐ │││
// │││ │ horizontalStackView                                                                                     │ │││
// │││ │┌─────────────────────────────────────────────────────────────────┐┌────────────────────────────────────┐│ │││
// │││ ││                                                                 ││                                    ││ │││
// │││ ││     titleLabel                                                  ││ phoneNumberLabel                   ││ │││
// │││ ││                                                                 ││                                    ││ │││
// │││ │└─────────────────────────────────────────────────────────────────┘└────────────────────────────────────┘│ │││
// │││ └─────────────────────────────────────────────────────────────────────────────────────────────────────────┘ │││
// │││ ┌─────────────────────────────────────────────────────────────────────────────────────────────────────────┐ │││
// │││ │     addressLabel                                                                                        │ │││
// │││ │                                                                                                         │ │││
// │││ └─────────────────────────────────────────────────────────────────────────────────────────────────────────┘ │││
// │││ ┌─────────────────────────────────────────────────────────────────────────────────────────────────────────┐ │││
// │││ │     mapView                                                                                             │ │││
// │││ │                                                                                                         │ │││
// │││ └─────────────────────────────────────────────────────────────────────────────────────────────────────────┘ │││
// │││ ┌─────────────────────────────────────────────────────────────────────────────────────────────────────────┐ │││
// │││ │     overviewTextView                                                                                    │ │││
// │││ │                                                                                                         │ │││
// │││ │                                                                                                         │ │││
// │││ │                                                                                                         │ │││
// │││ └─────────────────────────────────────────────────────────────────────────────────────────────────────────┘ │││
// ││└─────────────────────────────────────────────────────────────────────────────────────────────────────────────┘││
// │└───────────────────────────────────────────────────────────────────────────────────────────────────────────────┘│
// └─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘

class ListCell: UITableViewCell {
    
    static let identifier = "ListCell"
    
    private lazy var horizontalStackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .fill
        view.spacing = 5
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var verticalStackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.distribution = .fill
        view.spacing = 15
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var boxView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: Constant.backgroundColor)
        return view
    }()
    
    private let titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.preferredFont(forTextStyle: .headline)
        view.numberOfLines = 0
        view.textColor = UIColor(hexString: Constant.mainColor)
        return view
    }()
    
    private lazy var phoneNumberLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .right
        view.font = UIFont.systemFont(ofSize: 10)
        view.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(callNumber))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    private lazy var addressLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = UIFont.preferredFont(forTextStyle: .caption2)
        view.textColor = UIColor(hexString: Constant.subColor)
        return view
    }()
    
    private lazy var mapView: MKMapView = {
        let view = MKMapView()
        view.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        view.mapType = MKMapType.standard
        view.isZoomEnabled = true
        view.showsUserLocation = true
        view.isScrollEnabled = true
        view.layer.cornerRadius = 10
        return view
    }()
    
    private lazy var overviewTextView: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.clipsToBounds = true
        view.font = UIFont.systemFont(ofSize: 14)
        view.isMultipleTouchEnabled = false
        view.textAlignment = .natural
        view.textColor = UIColor.label
        view.isScrollEnabled = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = false
        view.textColor = UIColor(hexString: Constant.subColor2)
        view.sizeToFit()
        return view
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
        setupLayout()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        phoneNumberLabel.text = nil
        overviewTextView.text = nil
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        contentView.addSubview(boxView)
        
        boxView.addSubview(verticalStackView)
        
        verticalStackView.addArrangedSubview(horizontalStackView)
        verticalStackView.addArrangedSubview(addressLabel)
        verticalStackView.addArrangedSubview(mapView)
        verticalStackView.addArrangedSubview(overviewTextView)
        
        horizontalStackView.addArrangedSubview(titleLabel)
        horizontalStackView.addArrangedSubview(phoneNumberLabel)
    }
    
    private func setupLayout() {
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        boxView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        boxView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        boxView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        boxView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        
        verticalStackView.topAnchor.constraint(equalTo: boxView.topAnchor, constant: 10).isActive = true
        verticalStackView.bottomAnchor.constraint(equalTo: boxView.bottomAnchor, constant: -10).isActive = true
        verticalStackView.leadingAnchor.constraint(equalTo: boxView.leadingAnchor, constant: 10).isActive = true
        verticalStackView.trailingAnchor.constraint(equalTo: boxView.trailingAnchor, constant: -10).isActive = true
        
        horizontalStackView.leadingAnchor.constraint(equalTo: verticalStackView.leadingAnchor).isActive = true
        horizontalStackView.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor).isActive = true
        
        mapView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        phoneNumberLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
    }
    
    private func setupMap(latitude: Double, longitude: Double) {
        // Drop a pin at School's location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        mapView.addAnnotation(myAnnotation)
        
        // Zoom to School's location
        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.003, longitudeDelta: 0.003)
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    @objc private func callNumber() {
        guard let url = URL(string: "telprompt://\(String(describing: phoneNumberLabel.text))"),
              UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: Cell Configuration
    func configurateTheCell(_ data: School) {
        titleLabel.text = data.schoolName
        phoneNumberLabel.text = data.phoneNumber
        overviewTextView.text = data.overviewParagraph
        addressLabel.text = "\(data.primaryAddressLine1 ?? ""), \(data.city ?? "")"
        setupMap(latitude: Double(data.latitude ?? "0.0") ?? 0.0, longitude: Double(data.longitude ?? "0.0") ?? 0.0)
    }
}

