//
//  DetailViewController.swift
//  YohanYi
//
//  Created by Yohan Yi on 2/12/23.
//

import UIKit

class DetailViewController: UIViewController {
    
    var schoolDbn: String?
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    private lazy var cardView: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexString: Constant.backgroundColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }()
    
    private lazy var headerStackView: UIStackView = {
        var view = UIStackView()
        view.alignment = .top
        view.axis = .vertical
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var scoreStackView: UIStackView = {
        var view = UIStackView()
        view.axis = .vertical
        view.contentMode = .scaleAspectFill
        view.spacing = 40
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarSetup()
        setupActivityIndicator()
        fetchSATScore()
    }
    
    private func navigationBarSetup() {
        navigationItem.title = Constant.detailVCTitle
        view.backgroundColor = UIColor.systemBackground
        navigationController?.navigationBar.tintColor = UIColor(hexString: Constant.backgroundColor)
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: Constant.backgroundColor)]
    }
    
    private func setupActivityIndicator() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func fetchSATScore() {
        activityIndicator.startAnimating()
        WebServiceRequest.fetchingSATScoreList() { success, errorMessage, data in
            if success, let data = data as? [SATScore] {
                DispatchQueue.main.async {
                    if let i = data.firstIndex(where: { $0.dbn == self.schoolDbn }) {
                        // Found SAT Score
                        self.setupUI(SATScore: data[i])
                    } else {
                        // School dbn is not in the SAT list
                        self.setupUI(SATScore: nil)
                    }
                }
            } else {
                // network fail
                DispatchQueue.main.async {
                    self.setupUI(SATScore: nil)
                }
                print("Error :: \(String(describing: errorMessage))")
            }
        }
    }
    
    private func setupUI(SATScore: SATScore?) {
        self.activityIndicator.stopAnimating()
        
        guard let SATScore = SATScore else {
            // Error Screen
            let errorView = setupErrorViewUI()
            view.addSubview(errorView)
            errorView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
            errorView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
            errorView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
            return
        }
        
        view.addSubview(cardView)
        
        setupHeaderStackView(SATScore: SATScore)
        cardView.addSubview(headerStackView)
        cardView.addSubview(scoreStackView)
        
        let readingStackView = satScoreView(SATScore: SATScore, testType: .reading)
        scoreStackView.addArrangedSubview(readingStackView)
        let mathStackView = satScoreView(SATScore: SATScore, testType: .math)
        scoreStackView.addArrangedSubview(mathStackView)
        let writingStackView = satScoreView(SATScore: SATScore, testType: .writing)
        scoreStackView.addArrangedSubview(writingStackView)
        setupLayout()
    }
    
    private func setupLayout() {
        view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: 15).isActive = true
        cardView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        cardView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        
        headerStackView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 15).isActive = true
        scoreStackView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 15).isActive = true
        headerStackView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 15).isActive = true
        scoreStackView.topAnchor.constraint(equalTo: headerStackView.bottomAnchor, constant: 25).isActive = true
        
        cardView.bottomAnchor.constraint(equalTo: scoreStackView.bottomAnchor, constant: 25).isActive = true
        cardView.trailingAnchor.constraint(equalTo: headerStackView.trailingAnchor, constant: 15).isActive = true
        cardView.trailingAnchor.constraint(equalTo: scoreStackView.trailingAnchor, constant: 15).isActive = true
    }
    
    private func setupHeaderStackView(SATScore: SATScore) {
        lazy var labelSchoolNameLabel: UILabel = {
            let view = UILabel()
            view.contentMode = .left
            view.font = UIFont.systemFont(ofSize: 17, weight: .heavy)
            view.numberOfLines = 0
            view.textColor = UIColor.systemYellow
            view.translatesAutoresizingMaskIntoConstraints = false
            view.text = SATScore.schoolName
            return view
        }()
        
        lazy var labelNumOfTestTakerLabel: UILabel = {
            let view = UILabel()
            view.contentMode = .left
            view.font = UIFont.systemFont(ofSize: 12)
            view.numberOfLines = 1
            view.text = "Number of SAT test takers: \(SATScore.numOfSatTestTakers ?? "n/a")"
            view.textAlignment = .right
            view.textColor = UIColor(white: 0, alpha: 1)
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        headerStackView.addArrangedSubview(labelSchoolNameLabel)
        headerStackView.addArrangedSubview(labelNumOfTestTakerLabel)
    }
    
    private func satScoreView(SATScore: SATScore, testType: TestType) -> UIStackView {
        lazy var stackView: UIStackView = {
            let view = UIStackView()
            view.axis = .vertical
            view.contentMode = .scaleAspectFill
            view.spacing = 8
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        lazy var titleLabel: UILabel = {
            let view = UILabel()
            view.contentMode = .left
            view.font = UIFont.systemFont(ofSize: 17)
            view.textAlignment = .natural
            view.textColor = UIColor.white
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        lazy var scoreLabel: UILabel = {
            let view = UILabel()
            view.contentMode = .left
            view.font = UIFont.systemFont(ofSize: 17)
            view.textAlignment = .natural
            view.textColor = UIColor.white
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        lazy var scoreProgressView: UIProgressView = {
            let view = UIProgressView()
            view.progressTintColor = UIColor(hexString: Constant.mainColor)
            view.setContentHuggingPriority(.defaultHigh, for: .vertical)
            view.progressViewStyle = .default
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(scoreLabel)
        stackView.addArrangedSubview(scoreProgressView)
        
        var score: Float = 0.0
        switch testType {
        case .math:
            titleLabel.text = "Math Avg Score"
            scoreLabel.text = SATScore.satMathAvgScore
            score = (Float(SATScore.satMathAvgScore ?? "0.0") ?? 0)/800
        case .reading:
            titleLabel.text = "Critical Reading Avg Score"
            scoreLabel.text = SATScore.satCriticalReadingAvgScore
            score = (Float(SATScore.satCriticalReadingAvgScore ?? "0.0") ?? 0)/800
        case .writing:
            titleLabel.text = "Writing Avg Score"
            scoreLabel.text = SATScore.satWritingAvgScore
            score = (Float(SATScore.satWritingAvgScore ?? "0.0") ?? 0)/800
        }
        scoreProgressView.progress = score
        scoreProgressView.setProgress(scoreProgressView.progress, animated: true)
        
        return stackView
    }
    
    private func setupErrorViewUI() -> UIView{
        lazy var errorView = UIView()
        lazy var errorLabel = UILabel()
        errorView.addSubview(errorLabel)
        errorView.backgroundColor = UIColor(hexString: Constant.backgroundColor)
        errorView.layer.cornerRadius = 10
        errorView.translatesAutoresizingMaskIntoConstraints = false
        
        errorLabel.font = UIFont.systemFont(ofSize: 17, weight: .heavy)
        errorLabel.text = "Error: Can not find a score"
        errorLabel.textAlignment = .center
        errorLabel.textColor = UIColor.systemYellow
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        
        errorView.heightAnchor.constraint(equalToConstant: 378).isActive = true
        errorLabel.centerYAnchor.constraint(equalTo: errorView.centerYAnchor).isActive = true
        errorLabel.centerXAnchor.constraint(equalTo: errorView.centerXAnchor).isActive = true
        return errorView
    }
}
