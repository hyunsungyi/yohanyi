//
//  Webservice.swift
//  YohanYi
//
//  Created by Yohan Yi on 2/12/23.
//

import Foundation

class WebServiceRequest {
    
    static func fetchingNYCSchoolsList (completionHandler: @escaping (_ success: Bool, _ errorMessage: String?, _ data: Any?) -> Void) {
        let urlString = Constant.shoolListUrl
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completionHandler(false, "Failed to fetch list: \(error.localizedDescription)", nil)
                return
            }
            guard let data = data else { return }
            do {
                let response = try JSONDecoder().decode(NYCSchools.self, from: data)
                completionHandler(true, nil, response)
            } catch {
                completionHandler(false, "Failed to decode list: \(error.localizedDescription)", nil)
                return
            }
        }.resume()
    }
    
    static func fetchingSATScoreList (completionHandler: @escaping (_ success: Bool, _ errorMessage: String?, _ data: Any?) -> Void) {
        let urlString = Constant.satListUrl
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completionHandler(false, "Failed to fetch list: \(error.localizedDescription)", nil)
                return
            }
            guard let data = data else { return }
            do {
                let response = try JSONDecoder().decode(SATScoreList.self, from: data)
                completionHandler(true, nil, response)
            } catch {
                completionHandler(false, "Failed to decode list: \(error.localizedDescription)", nil)
                return
            }
        }.resume()
    }
}
